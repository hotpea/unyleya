<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

//auth
$app->post('/auth/login', 'AuthController@loginPost');

//cars
$app->get('/api/cars', 'CarsController@index');
$app->post('/api/cars/show', 'CarsController@show');
$app->post('/api/cars/store', 'CarsController@store');
$app->post('/api/cars/update', 'CarsController@update');
$app->post('/api/cars/destroy', 'CarsController@destroy');

//accessories
$app->get('/api/accessories', 'AccessoriesController@index');
$app->post('/api/accessories/show', 'AccessoriesController@show');
$app->post('/api/accessories/store', 'AccessoriesController@store');
$app->post('/api/accessories/update', 'AccessoriesController@update');
$app->post('/api/accessories/destroy', 'AccessoriesController@destroy');

//userss
$app->get('/api/users', 'UsersController@index');
$app->post('/api/users/show', 'UsersController@show');
$app->post('/api/users/store', 'UsersController@store');
$app->post('/api/users/update', 'UsersController@update');
$app->post('/api/users/destroy', 'UsersController@destroy');