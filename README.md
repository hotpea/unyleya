# API de CRUD de carros e seus opcionais

Essa é uma API com operações básicas de CRUD de carros e seus opcionais, construída com PHP7, LUMEN 5.3, MySQL e Swagger 2.0
Obs¹: foi desenvolvida em um ambiente com PHP7, mas não foi usado nada específico desta versão, então, creio que funcione em qualquer versão não inferior a 5.6.

  - LUMEN 5.3
  - MySQL 
  - Swagger 2.0
  - PHP 7.1(compatível com PHP ^5.6)

# Pré requisitos

Algumas coisas são necessárias para iniciarmos com este projeto:

  - PHP 5.6 ou superior
  - Uma conexão MySQL
  - Composer
 
# Instalação

Faça o clone deste projeto, em seguida, acesse o diretório aonde fez o clone.
Na raiz do projeto está o arquivo ".env" onde se encontrarm alguns dados de configuração do Lumen, entre esses dados, temos os dados de conexão que, por padrão são os seguintes:

<p>DB_CONNECTION=mysql</p>
<p>DB_HOST=127.0.0.1</p>
<p>DB_PORT=3306</p>
<p>DB_DATABASE=unyleya</p>
<p>DB_USERNAME=root</p>
<p>DB_PASSWORD=</p>

Esses dados podem ser alterados para a sua configuração MySQL ou, pode ser criado uma nova conexão para bater com essa já existente.

Após os dados de conexão devidamente configurados e base de dados devidamente crida, na raiz do projeto, execute os comandos:

* php artisan migrate
* php artisan db:seed

logo em seguida, inicie o serviço com o comando:

* php -S localhost:8000 -t public

Obs²: é necessário uma versão do PHP que possua um servidor embutido para o comando acima funcionar, caso contrário, se faz neessário a configuração de um ambiente padrão, com um servidor de aplicação.

após isso, basta acessar o sistema em seu navegador favorito!
pello endereço "http://localhost:8000/api/documentation".

Atenciosamente, 

Stéfan de Lima Amaral.
Analista e Desenvolvedor de Softwares.
https://www.linkedin.com/in/st%C3%A9fan-de-lima-amaral-13325982/