<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Car extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'model_name', 'model_year',
    ];

    public function accessories()
    {
        return $this->belongsToMany('App\Accessory');
    }
}
