<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Accessory;
class AccessoriesController extends Controller
{
    /**
     * impede que o controller seja consumido sem autenticação
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Recupera todos os registros
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accessories = Accessory::with('cars')->get()->all();
        return response()->json($accessories);
    }

    /**
     * Retorna um registro específico pelo id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $accessory = Accessory::where('id', $request->id)->with('cars')->get()->all();
        if(!empty($accessory)){
            return response()->json($accessory);
        }
        else{
            return response()->json(['status' => 'fail']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'accessory_name' => 'required',
            'optional' => 'required',
        ]);
        $accessory = new Accessory();

        $accessory->accessory_name = $request->accessory_name;
        $accessory->optional = ($request->optional == 'true');

        $accessory->save();
        return response()->json(['status' => 'success']);

    }

    /**
     * Atualiza um registro pelo id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'accessory_name' => 'required',
            'optional' => 'required',
        ]);

        $accessory = Accessory::find($request->id);
        $accessory->accessory_name = $request->accessory_name;
        $accessory->optional = ($request->optional == 'true');

        $accessory->save();
        return response()->json(['status' => 'success']);
    }

    /**
     * Remove um registro pelo id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $accessory = Accessory::find($request->id);
        if($accessory){
            $accessory->delete();
            return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'false']);
        }
    }
}