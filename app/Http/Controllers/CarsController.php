<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car;
use App\Accessory;
class CarsController extends Controller
{
    /**
     * impede que o controller seja consumido sem autenticação
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Recupera todos os registros
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = Car::with('accessories')->get()->all();
        return response()->json($cars);
    }

    /**
     * Retorna um registro específico pelo id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $car = Car::where('id', $request->id)->with('accessories')->get()->all();
        if(!empty($car)){
            return response()->json($car);
        }
        else{
            return response()->json(['status' => 'fail']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'model_name' => 'required',
            'model_year' => 'required',
            'wheel_size' => 'required',
            'accessories' => 'required',
        ]);

        $car = new Car();
        $car->model_name = $request->model_name;
        $car->model_year = $request->model_year;
        $car->wheel_size = $request->wheel_size;

        $car->save();

        foreach($request->accessories as $accessory) {
            if( !($this->validateAccessory($accessory['id'])) ){
                Car::destroy($car->id);
                return response()->json([
                    'status' => 'fail',
                    'message' => 'accessory not exists'
                ]);
            }
            $car->accessories()->attach($accessory['id']);
        }

        return response()->json(['status' => 'success']);
    }

    /**
     * Atualiza um registro pelo id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {

        $this->validate($request, [
            'id' => 'required',
            'model_name' => 'required',
            'model_year' => 'required',
            'wheel_size' => 'required',
            'accessories' => 'required',
        ]);

        $car = Car::find($request->id);

        // valida se o id dado retornou um registro
        if($car == null){
            return response()->json([
                'status' => 'fail',
                'message' => 'this id not exists for cars'
            ]);
        }

        // desvinculando todos os accessórios para guardar os novos
        $car->accessories()->detach();

        foreach($request->accessories as $accessory) {
            if( !($this->validateAccessory($accessory['id'])) ){
                return response()->json([
                    'status' => 'fail',
                    'message' => 'accessory not exists'
                ]);
            }
            $car->accessories()->attach($accessory['id']);
        }

        $car->model_name = $request->model_name;
        $car->model_year = $request->model_year;
        $car->wheel_size = $request->wheel_size;

        $car->save();
        return response()->json(['status' => 'success']);
    }

    /**
     * Remove um registro pelo id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        // desvincula todos os acessórios antes de deletar o carro
        Car::find($request->id)->accessories()->detach();

        if(Car::destroy($request->id)){
            return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'false']);
        }
    }

    public function validateAccessory($accessory_id){
        $accessory = Accessory::find($accessory_id);
        if($accessory == null) {
            return false;
        }
        return true;
    }
}