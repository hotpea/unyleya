<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Tymon\JWTAuth\JWTAuth;

class UsersController extends Controller
{
    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    /**
     * Recupera todos os registros
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return response()->json($users);
    }

    /**
     * Retorna um registro específico pelo id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $user = User::where('id', $request->id)->get()->all();
        if(!empty($user)){
            return response()->json($user);
        }
        else{
            return response()->json(['status' => 'fail']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = app('hash')->make($request->password);

        $user->save();

        return response()->json(['status' => 'success']);
    }

    /**
     * Atualiza um registro pelo id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {

        $this->validate($request, [
            'id' => 'required',
            'name' => 'required',
            'old_email' => 'required',
            'email' => 'required',
            'old_password' => 'required',
            'password' => 'required',
        ]);

        if (! $token = $this->jwt->attempt(['email' => $request->old_email, 'password' => $request->old_password])) {
            return response()->json(['email ou senha anteriores inválidos'], 403);
        }

        $user = User::find($request->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = app('hash')->make($request->password);

        $user->save();

        return response()->json(['status' => 'success']);
    }

    /**
     * Remove um registro pelo id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        if(User::destroy($request->id)){
            return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'false']);
        }
    }

}