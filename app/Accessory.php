<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Accessory extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'accessory_name', 'optional ',
    ];

    public function cars()
    {
        return $this->belongsToMany('App\Car');
    }
}
